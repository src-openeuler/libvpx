Name:                   libvpx
Version:                1.15.0
Release:                1
Summary:                VP8/VP9 Video Codec SDK
License:                BSD-3-Clause
Source0:                https://github.com/webmproject/libvpx/archive/v%{version}.tar.gz
Patch0:                 add-riscv64-arch.patch
URL:                    http://www.webmproject.org/code/
BuildRequires:          gcc gcc-c++ doxygen, perl(Getopt::Long)
%ifarch %{ix86} x86_64
BuildRequires:          nasm
%endif
Provides:               %{name}-utils = %{version}-%{release}
Obsoletes:              %{name}-utils < %{version}-%{release}

%description
libvpx provides the VP8/VP9 SDK, which allows you to integrate your applications
with the VP8 and VP9 video codecs, high quality, royalty free, open source codecs
deployed on millions of computers and devices worldwide.

%package devel
Summary:                Development files for libvpx
Requires:               %{name} = %{version}-%{release}

%description devel
Development libraries and headers for developing software against libvpx.

%prep
%autosetup libvpx-%{version} -p1

%build
%global vpxtarget generic-gnu
%ifarch x86_64
%global vpxtarget x86_64-linux-gcc
%endif
%ifarch aarch64
%global vpxtarget arm64-linux-gcc
%endif
%ifarch riscv64
%global vpxtarget riscv64-linux-gcc
%endif
%ifarch loongarch64
%global vpxtarget loongarch64-linux-gcc
%endif

%set_build_flags
./configure --target=%{vpxtarget} \
    --prefix=%{_prefix} \
    --disable-install-srcs \
    --enable-install-docs \
    --enable-docs \
    --size-limit=16384x16384 \
    --disable-unit-tests \
    --enable-pic \
    --enable-postproc \
    --enable-runtime-cpu-detect \
    --enable-shared \
    --disable-static \
%ifarch %{ix86} x86_64
    --as=nasm \
%endif
    --enable-vp8 \
    --enable-vp9 \
    --enable-vp9-highbitdepth \
    --enable-vp9-temporal-denoising

sed -i "s|-O3|%{optflags}|g" libs-%{vpxtarget}.mk
sed -i "s|-O3|%{optflags}|g" examples-%{vpxtarget}.mk
sed -i "s|-O3|%{optflags}|g" docs-%{vpxtarget}.mk

%make_build verbose=true

%install
%make_install LIBSUBDIR=%{_lib}

cp -a vpx_config.h %{buildroot}%{_includedir}/vpx/vpx_config-%{_arch}.h
touch -r AUTHORS %{buildroot}%{_includedir}/vpx/vpx_config.h

%files
%license LICENSE
%doc AUTHORS CHANGELOG README
%{_libdir}/libvpx.so.*
%{_bindir}/*

%files devel
%doc docs/html
%{_includedir}/vpx
%{_libdir}/pkgconfig/vpx.pc
%{_libdir}/libvpx.so

%changelog
* Mon Dec 9 2024 xiejing <xiejing@kylinos.cn> - 1.15.0-1
- Update to 1.15.0 version
- This release includes new codec control for key frame filtering, more Neon
  optimizations, improvements to RTC encoding and bug fixes

* Tue Oct 29 2024 Funda Wang <fundawang@yeah.net> - 1.14.1-2
- build with more active nasm
- build same results with openSUSE and debian, drop examples
- disable unit tests as it does build with lto
- cleanup spec

* Thu Jun 06 2024 kywqs  <weiqingsong@kylinos.cn> - 1.14.1-1
- Update to 1.14.1 version
- Fix CVE-2024-5197

* Sat Nov 25 2023 Jingwiw  <wangjingwei@iscas.ac.cn> - 1.13.1-1
- Update to 1.13.1 version
- Migrate to SPDX license

* Sun Oct 01 2023 Funda Wang <fundawang@yeah.net> - 1.12.0-4
- Fix CVE-2023-5217, CVE-2023-44488

* Wed May 17 2023 laokz <zhangkai@iscas.ac.cn> - 1.12.0-3
- Add RISC-V arch support authored-by: YukariChiba <i@0x7f.cc>

* Mon May  8 2023 Wenlong Zhang <zhangwenlong@loongson.cn> - 1.12.0-2
- fix build error for loongarch64

* Sat Feb 04 2023 wenchaofan <349464272@qq.com> - 1.12.0-1
- Update to 1.12.0 version

* Fri Nov 08 2019 Lijin Yang <yanglijin@huawei.com> -1.7.0-8
- Pakcage init

